# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
#import os
#this_server = os.getenv('HACKX_SERVER_TYPE', 'apiserver')
this_server='apiserver'
#this_server='lb'

servers = ['52.76.7.16','52.76.35.162','52.76.59.147']

import json


def bhai_log(s):
	open('bhai.log', 'a').write(str(s)+'\n')

def hasKey(request):
	if len(request.args)>0 and len(request.args[0]) > 0:
		return True
	else:
		return False

def getKey(request):
	return str(request.args[0])

def handleKey(request):
	if not hasKey(request):
		raise HTTP(400, "Bad key argument.")
	else:
		return getKey(request)

def getPayload(request):
	if len(request.args)>1:
		return request.args[1]
	else:
		return request.body.read() 

def getcll(request):
	c = request.vars.client if request.vars.client else '0'
	lt = float(request.vars.lat) if request.vars.lat else 0.0
	lg = float(request.vars.lng) if request.vars.lng else 0.0
	return c,lt,lg
	
def handlePayload(request):
	ret = getPayload(request)
	if len(ret)==0:
		raise HTTP(403, "No payload attached")
	else:
		return ret


def loadBalance(func):
	if this_server=="lb":
		def lb2():
			import multiprocessing
			def c(result_queue, skey, request, pp):
				import requests
				this_url = 'http://'+servers[skey%len(servers)]+request.env.web2py_original_uri
				try:
					res = requests.request('POST', this_url, data=pp, timeout=3)
				except:
					result_queue.put(this_url)
					return
				result_queue.put(res)

			keyy = handleKey(request)
			skey = hash(keyy)%len(servers)
			pp = getPayload(request)
			processes = []
			result_queue = multiprocessing.Queue()
			for i in range(2):
				p = multiprocessing.Process(target=c, args=[result_queue, skey+i, request, pp])
				p.start()
				processes.append(p)
			res = result_queue.get()
			for p in processes:
				p.join()
			try:
				res.status_code
			except:
				return res
			if res.status_code==200:
				return res
			else:
				raise HTTP(res.status_code, res.content)
		return lb2
	else:
		return func

def getOffsetSz(request):
	try:
		offset = int(request.vars.offset)
	except:
		offset = 0

	try:
		sz = int(request.vars.size)
	except:
		sz = 1
	return offset,sz
	
@loadBalance
def get():
	ret = ""
	if hasKey(request):
		keyy = getKey(request)
		ret = db(db.videos.key_==keyy).select()
		if len(ret) < 1:
			raise HTTP(404,"Key not found in database.")
	else:
		ret = db().select(db.videos.ALL, distinct=True)
	t = ''
	for r in ret:
		t+= r.segment
	return t
	#return json.dumps(dict(status=status, count = len(ret), ret=ret.as_list()))

@loadBalance
def get_key_data():
	return get_key_data_helper()

def get_key_data_helper():
	ret = ""
	if hasKey(request):
		keyy = getKey(request)
		ret = db(db.videos.key_==keyy).select()
		if len(ret) < 1:
			raise HTTP(404,"Key not found in database.")
	else:
		ret = db().select(db.videos.ALL, distinct=True)
	t = ''
	for r in ret:
		t+= r.segment
	return json.dumps(dict(data=t, lang=ret.first().lng, lat=ret.first().lat, r=ret.first().as_dict(), sz=len(t)))

def c_key(keyy):
	t = ''
	ret = db(db.videos.key_==keyy).select()
	for r in ret:
		t+=r.segment
	return t,ret.first().as_dict()


@loadBalance
def get_near():
	arr = []
	rr = db(db.videos.segment_id==0).select(db.videos.key_, limitby=(0,3))
	for r in rr:
		arr.append(c_key(r.key_))
	return json.dumps(dict(arr=arr))

@loadBalance
def put():
	handleKey(request)
	keyy = getKey(request)
	payload = handlePayload(request)
	c,lt,lg = getcll(request)
	#payload = getPayload(request)
	sz = len(payload) if payload else 0
	if False: #db(db.videos.key_==keyy)(db.videos.segment_id==0).count()>0:
		raise HTTP(402, "Key already exist")
	else:
		if db(db.videos.key_==keyy).count()>0:
			db(db.videos.key_==keyy).delete()
		new_id = db.videos.insert(key_=keyy, segment=payload, sz=sz, start_offset=0, end_offset=sz, 
		client=c, lat=lt, lng=lg)
	#return json.dumps(dict(status="Success", new_id=new_id))

@loadBalance
def get_next():
	keyy = handleKey(request)
	offset,sz = getOffsetSz(request)
	ret = db(db.videos.key_==keyy).select()
	if len(ret) < 1:
		raise HTTP(404,"Key not found in database.")
	ret = db((db.videos.key_==keyy) & ~(db.videos.end_offset<=offset) & ~(db.videos.start_offset>offset+sz-1)).select()
	if len(ret) < 1:
		raise HTTP(404)
	#return ret
	#retf = ret.first()
	#d = str(retf.segment)[offset - retf.start_offset:offset - retf.start_offset + sz]
	t = ''
	s = 0
	for r in ret:
		if r.start_offset>=offset:
			rseg = r.segment
		else:
			rseg = r.segment[r.start_offset - offset:]
		if s + r.sz < sz:
			t+= rseg
		else:
			t+= rseg[:sz-s]
		s += len(rseg)
	return t
	#return json.dumps(dict(status="Success", data=d))

@loadBalance
def put_next():
	keyy = handleKey(request)
	offset,sz = getOffsetSz(request)
	ret = db(db.videos.key_==keyy).select()
	payload = handlePayload(request)
	c,lt,lg = getcll(request)

	sz = len(payload)
	if len(ret) < 1:
		new_id = db.videos.insert(key_=keyy, segment=payload, sz=sz, start_offset=0, 
		end_offset=sz,
		client=c, lat=lt, lng=lg
		)
	else:
		ff = db(db.videos.key_==keyy).select(db.videos.end_offset.max().with_alias('m'),
		db.videos.client, db.videos.lat, db.videos.lng).first()
		so = ff.m
		ff = db(db.videos.key_==keyy)(db.videos.segment_id==0).select(db.videos.ALL).first()
		c = ff.client
		lt = ff.lat
		lg = ff.lng
		new_id = db.videos.insert(key_=keyy, segment=payload, sz=sz, start_offset=so, 
		end_offset=so+ sz, segment_id=len(ret),
		client=c, lat=lt, lng=lg
		)
	#return json.dumps(dict(status="Success", new_id=new_id))

@loadBalance
def delete():
	keyy = handleKey(request)
	ret = db(db.videos.key_==keyy).delete()
	#return json.dumps(dict(status="Success", ret=ret))



def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


